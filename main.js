const modalOpen = document.querySelector("#modal-open");
const modalClose = document.querySelector("#close-modal");
const modal = document.querySelector("#modal");
const container = document.querySelector("#container");

/*
 not using css framework like 
 bootstrap or material and NOT having these
 set, causes sometimes band calculation of 
 height or width on mobile chrome
 */

document.body.style.height = window.innerHeight + "px";
document.body.style.width = window.innerWidth + "px";

// Attach eventListener on open and close modal

modalOpen.addEventListener("click", () => {
  modal.style.display = "flex";
  container.style.filter = "blur(5px)";
});

modalClose.addEventListener("click", () => {
  modal.style.display = "none";
  container.style.filter = "blur(0px)";
});
